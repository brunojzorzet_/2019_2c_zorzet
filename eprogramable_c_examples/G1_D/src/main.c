/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

#define OUT 1
#define IN 0
/*==================[macros and definitions]=================================*/

typedef struct
{  uint8_t port;//numero del puerto
	uint8_t pin;
	uint8_t dir;}gpioConf_t;


/*==================[internal functions declaration]=========================*/


void mapeoPuertos(gpioConf_t *puertos, uint8_t bcd){
	/*uint8_t m1=bcd&(1<<0);
	uint8_t m2=(bcd&(1<<1))>>1;
	uint8_t m3=(bcd&(1<<2))>>2;
	uint8_t m4=(bcd&(1<<3))>>3;
	*/uint8_t m;


	uint8_t i;
	for(i=0;i<4;i++){
		m=((bcd&(1<<i))>>i);

		if(m==1){
				printf("El puerto %d ",puertos[i].port);
				printf("posee el pin %d ",puertos[i].pin);
				printf("En un 1 logico\n");}
			else {
				printf("El puerto %d ",puertos[i].port);
						printf("posee el pin %d ",puertos[i].pin);
						printf("En un 0 logico\n");	}
		}}

	/*if(m1==1){
		printf("El puerto %d ",puertos[0].port);
		printf("posee el pin %d ",puertos[0].pin);
		printf("En un 1 logico\n");}
	else {
		printf("El puerto %d ",puertos[0].port);
				printf("posee el pin %d ",puertos[0].pin);
				printf("En un 0 logico\n");
	}

	if(m2==1){
		printf("El puerto %d ",puertos[1].port);
		printf("posee el pin %d ",puertos[1].pin);
		printf("En un 1 logico\n");}
	else {
		printf("El puerto %d ",puertos[1].port);
				printf("posee el pin %d ",puertos[1].pin);
				printf("En un 0 logico\n");
	}

	if(m3==1){
		printf("El puerto %d ",puertos[2].port);
		printf("posee el pin %d ",puertos[2].pin);
		printf("En un 1 logico\n");}
	else {
		printf("El puerto %d ",puertos[2].port);
				printf("posee el pin %d ",puertos[2].pin);
				printf("En un 0 logico\n");
	}

	if(m4==1){
		printf("El puerto %d ",puertos[3].port);
		printf("posee el pin %d ",puertos[3].pin);
		printf("En un 1 logico");}
	else {
		printf("El puerto %d ",puertos[3].port);
				printf("posee el pin %d ",puertos[3].pin);
				printf("En un 0 logico");
	}}*/

gpioConf_t BRUNO [4]={
		{1,4,OUT},{1,5,OUT},{1,6,OUT},{2,14,OUT},};



int main(void)
{
	uint8_t numero=(1<<3)|(0<<2)|(0<<1)|(1<<0);
   mapeoPuertos(BRUNO,numero);
	return 0;
}

/*==================[end of file]============================================*/

