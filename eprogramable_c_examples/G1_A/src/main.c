/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 2
#define TOGGLE 3




typedef struct {
	uint8_t n_led ;//numero de led a prender
	uint8_t n_ciclos; //indica la cantidad de ciclos de prendido y apagado
	uint8_t periodo;//indica el periodo de cada ciclo
	uint8_t mode; //ON, OFF, TOGGLE
	}my_leds;





/*==================[internal functions declaration]=========================*/

void prendeled (my_leds *led){

	if(led->mode==ON){
		if(led->n_led==1){
			printf("El led prendido en modo ON es el 1\n");
		    }
		if(led->n_led==2){
					printf("El led prendido en modo ON es el 2\n");
					}
		if(led->n_led==3){
					printf("El led prendido en modo ON es el 3\n");
					}}
	if(led->mode==OFF){
		if(led->n_led==1){
			printf("El led en modo OFF es el 1\n");
		    }
		if(led->n_led==2){
			printf("El led en modo OFF es el 2\n");
		    }
		if(led->n_led==3){
			printf("El led en modo OFF es el 3\n");
		    }}
	if(led->mode == TOGGLE){
		uint8_t i=0;
		uint8_t j=0;
		while(i<led->n_ciclos){
			if(led->n_led==1){
						printf("El led prendido en modo TOGGLE es el 1\n");
					    }
					if(led->n_led==2){
								printf("El led prendido en TOGGLE ON es el 2\n");
								}
					if(led->n_led==3){
								printf("El led prendido en TOGGLE ON es el 3\n");
								}
					j=0;
		while(j<led->periodo){
			j++;}}}


};



int main(void)
{
    my_leds leds;
    leds.mode=TOGGLE;
    leds.n_ciclos=5;
    leds.n_led=2;
    leds.periodo=2;
    prendeled(&leds);
	return 0;
}
//HAY UN PROBLEMA EN EL CUAL NO ME SALE DEL CICLO WHILE
/*==================[end of file]============================================*/

