/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

/*==================[macros and definitions]=================================*/

typedef struct {
	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
	}cada_byte;

	union data{
		cada_byte bytes;
		uint32_t num;}valor;


/*union test{
struct cada_byte{
uint8_t byte1;
uint8_t byte2;
uint8_t byte3;
uint8_t byte4;
};
uint32_t todos_los_bytes;
}*/

/*==================[internal functions declaration]=========================*/

int main(void)
{
    uint32_t var=0x01020304;
    uint16_t b1;
    uint16_t b2;

    b2=(uint16_t)var;
    var=var>>16;
    b1=(uint16_t) var;

    uint8_t a2=(uint8_t)b1;
    b1=b1>>8;

    uint8_t a1=(uint8_t)b1;

    uint8_t a4=(uint8_t)b2;
    b2=b2>>8;
    uint8_t a3=(uint8_t)b2;



    printf("El valor es %d\n",a1);
    printf("El valor es %d\n",a2);
    printf("El valor es %d\n",a3);
    printf("El valor es %d\n",a4);

    valor.num=0x01020304;
    printf("El valor es %d\n",valor.bytes.byte1);
    printf("El valor es %d\n",valor.bytes.byte2);
    printf("El valor es %d\n",valor.bytes.byte3);
    printf("El valor es %d\n",valor.bytes.byte4);

	return 0;
}

/*==================[end of file]============================================*/

