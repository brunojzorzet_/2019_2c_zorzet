/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
/*==================[macros and definitions]=================================*/
typedef struct{
	char *txt;
	void (*doit)();//Para declarar funciones debe colocarse de ersa manera
	}menuEntry;




/*==================[internal functions declaration]=========================*/
	void funcion1()	{
		printf("LA FUNCION ELEGIDA ES LA UNO\n");
	}
	void funcion2()	{
		printf("LA FUNCION ELEGIDA ES LA DOS?\n");}

	void funcion()	{
		printf("LA FUNCION ELEGIDA ES LA TRES\n");
	}
	void EjecutarMenu(menuEntry *menu, int opt){
		/*if(opt==1){
			printf("La opcion tiene la etiqueta de  %s", menu->txt);
			menu->doit();//Para que me ejecute la funcion hay que ponerle los parentesis
			};

		if(opt==2){
				printf("La opcion tiene la etiqueta de  %s", menu->txt);
				menu->doit();//Para que me ejecute la funcion hay que ponerle los parentesis
				};
		if(opt==3){
				printf("La opcion tiene la etiqueta de  %s", menu->txt);
				menu->doit();//Para que me ejecute la funcion hay que ponerle los parentesis
				};*/

		printf("La opcion tiene la etiqueta de  %s", menu[opt].txt);//Lo que ocurre es que tenemos un puntero
		//a un arreglo de strcut al colocar los corchetes se desreferencian y por tal motivo utilizamos el punto
		menu[opt].doit();

	}



	menuEntry menuPrincipal[3]={
	{"OPCION1",funcion1},{"OPCION2",funcion2},{"21",funcion}
	};


int main(void)
{
	uint8_t num=0;
	EjecutarMenu(menuPrincipal, num);

	return 0;
}

/*==================[end of file]============================================*/

