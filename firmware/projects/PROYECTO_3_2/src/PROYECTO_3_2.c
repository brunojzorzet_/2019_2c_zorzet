/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/* Esta aplicacion lo que realiza es lo mismo que el proyecto 2 pero utilizando TIMERS*/




/*==================[inclusions]=============================================*/
#include "../../PROYECTO_3_2/inc/PROYECTO_3_2.h"       /* <= own header */
#include "switch.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "bool.h"
#include "delay.h"
#include "led.h"
#include "timer.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/
#define RETARDO 100
#define BLANCO 0
#define ANTI_REBOTE 100
#define VEL_TRANSF 115200
#define VEL_TRANS_BLUETOOTH 9600
#define DECIMAL 10




/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

		//INTERRUPCIONES DEL SWITCHES
void Hold(void);				//CAMBIA EL ESTADO DE MI BANDERA HOLD
void UnitCm(void);				//CAMBIA EL ESTADO DE MI BANDERA A CM
void UnitInches(void);			//CAMBIA EL ESTADO DE MI BANDERA A PULGADAS
		//INTERRUPCION TIMER
void Refresco(void);			//CAMBIA DE ESTADO MI BANDERA REF_ACT
		//INTERRUPCION DE LA LECTURA DEL SERIE
void Input(void);				//RECIBE DATOS POR PARTE DEL CANAL DE COMUNICACION, X APAGA MI DISPOSITIVO, C CAMBIA A CM E I CAMBIA A PULGADAS

		//FUNCIONES
void MedicionAct(void);			//CAMBIA EL ESTADO DE MI BANDERA TECLA 1, A SU VEZ INICIALIZA AL TIMER
void PrenderRGB(void);			//PRENDER LED RGB
void ApagarLeds(void);			//APAGA TODOS LOS LEDS
void Medicion(void);			//Calcula la medida censada por el sonar y envia la informacion a los dispositivos LCD y de comunicacion


/*==================[external data definition]===============================*/
bool estado_T1=false;												//Bandera que verifica el estado de la tecla 1
bool unidad=true;													//TRUE= CM ........ FALSE=INCHES
bool hold=false;													// Bandera que verifica el estado de la tecla 2
bool ref_act=true;													// Bandera que activa el refresco cada 1 segundo
uint16_t medida=0;													// Distancia que censa mi sonar
serial_config ComunicationChannel_1={SERIAL_PORT_RS485,VEL_TRANSF,Input}; //INICIALIZACION DEL PUERTO DE COMUNICACION SERIE
serial_config ComunicationChannel_2={SERIAL_PORT_P2_CONNECTOR,VEL_TRANS_BLUETOOTH,Input}; //INICIALIZACION DEL PUERTO DE COMUNICACION BLUETOOTH
timer_config refresh={TIMER_B,1000,Refresco};	//Definimos el TIMER de 1 segundo, el cual debe de ejecutar refresco

/*==================[external functions definition]==========================*/
//INTERRUPCIONES SWITCHES
//CAMBIA EL ESTADO DE MI BANDERA HOLD
void Hold(void){
	hold=!hold;
}
//CAMBIA EL ESTADO DE MI BANDERA A CM
void UnitCm(void){
	unidad=true;
}
//CAMBIA EL ESTADO DE MI BANDERA A PULGADAS
void UnitInches(void){
	unidad=false;
}

//INTERRUPCIONES TIMER
void Refresco(void){
	ref_act=!ref_act;
}
//INTERRUPCIONES UART
void Input(void){
	uint8_t dato;
	UartReadByte(ComunicationChannel_1.port,&dato);   //Llamada a la funcion de lectura que lee el dato almacenado luego de recibir el dato
	switch(dato){
		case 'c':
			unidad=true;
		break;
		case 'i':
			unidad=false;
		break;
		case 'x':
			estado_T1=false;
		break;
	}
}
	//CAMBIA EL ESTADO DE MI BANDERA TECLA 1, A SU VEZ INICIALIZA AL TIMER

//FUNCIONES
	//CAMBIA EL ESTADO DE MI BANDERA TECLA 1, A SU VEZ INICIALIZA AL TIMER
void MedicionAct(void){
	estado_T1=!estado_T1;
	if(estado_T1==true){
		TimerStart(refresh.timer);
	}
	else{
		TimerStop(refresh.timer);
	}
}
	//APAGA TODOS LOS LEDS
void ApagarLeds(void){
	LedOff(LED_RGB_B);
	LedOff(LED_RGB_G);
	LedOff(LED_RGB_R);
	LedOff(LED_3);
	LedOff(LED_2);
	LedOff(LED_1);
}
	//PRENDER LED RGB
void PrenderRGB(void){
	LedOn(LED_RGB_B);										//Se prende en LED RGB, indicando la activacion de mi dispositivo
	LedOn(LED_RGB_R);
	LedOn(LED_RGB_G);
}
	//Calcula la medida censada por el sonar y envia la informacion a los dispositivos LCD y de comunicacion
void Medicion(void){
	if(unidad==true){
	   LedOff(LED_3);										//apaga el LED que indica la medicion en pulgadas
	   medida=HcSr04ReadDistanceCentimeters();				//LEEMOS LA MEDIDA
	   UartSendString(ComunicationChannel_1.port,UartItoa(medida,DECIMAL));  //enviamos el valor de la medida por el canal de comunicacion
	   UartSendString(ComunicationChannel_1.port," cm \r\n");
	   LedOn(LED_2);										//prendemos el LED indicador de cm
	}
	else{
	  LedOff(LED_2);										//apagamos el led indicador de cm
	  medida=HcSr04ReadDistanceInches();					//leemos la medida en pulgadas
	  UartSendString(ComunicationChannel_1.port,UartItoa(medida,DECIMAL));  //enviamos el valor de la medida por el canal de comunicacion
	  UartSendString(ComunicationChannel_1.port," in \r\n");
	  LedOn(LED_3);										//prendemos el led indicador de pulgadas
	}

	ITSE0803DisplayValue(medida);
}





int main(void){

	//INICIALIZACION DE LOS DRIVERS DE DISPOSITIVOS
	SystemClockInit();
	UartInit(&ComunicationChannel_1);
	UartInit(&ComunicationChannel_2);
	HcSr04Init(T_FIL2,T_FIL3); 											//inicializamos el sensor con los pines correspondientes
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5};				//definimos los pines que utilizara mi display
	ITSE0803Init(pines);	 											//inicializamos los pines para la pantalla
	SwitchesInit();														//inicializamos los switches
	LedsInit();															//inicializamos switches y leds
	TimerInit(&refresh);												//inicializamos el timer

	//DECLARAMOS LAS VARIABLES A UTILIZAR


	//INTERRUPCIONES
			//SWITCHES
	SwitchActivInt(SWITCH_1, MedicionAct);
	SwitchActivInt(SWITCH_2, Hold);
	SwitchActivInt(SWITCH_3, UnitCm);
	SwitchActivInt(SWITCH_4, UnitInches);

    while(1){
    	if(ref_act==true){					//Verifica que se deba realizar el refresco
    		if(hold==false){				//Verifica que no este activa la tecla HOLD
    			LedOff(LED_1);
    			PrenderRGB();				//Se prende en LED RGB, indicando la activacion de mi dispositivo
    			Medicion();					//Calcula la medida censada por el sonar y envia la informacion a los dispositivos LCD y de comunicacion
    		}
        	else{							//Se encuentra activado el HOLD
        		LedOn(LED_1);				//Se prende el led HOLD
        		ITSE0803DisplayValue(medida);//Se dispone la medida en el Display
        	}

    	ref_act=false;						//con esta linea nos aseguramos de que no se ejecute la medida en menos de 1 segundo
    	}//cierre del if(ref_act)
    	if(estado_T1==false){
    		ApagarLeds();
			ITSE0803DisplayValue(BLANCO);
		}
    }//cierre del while


	return 0;
}

/*==================[end of file]============================================*/

