/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* EN EL SIGUIENTE PROGRAMA PERMITE MEDIR DISTANCIAS ATRAVES DE ULTRSONIDO
 * LAS DISTANCIAS PUEDEN PRESENTARSE EN CENTIMETROS Y EN PULGADAS DEPENDIENDO DE
 * LAS TECLAS QUE PRESIONEMOS(TECLA 3 PARA CM Y TECLA 4 PARA PULGADAS).
 * TAMBIEN EL PROGRAMA PERMITE MANTENER UNA MEDIDA EN LA PANTALLA (TECLA 2).
 *
 * EL PRENDIDO DE LA APLICACION SE VERIFICA CON LEDS, ASI COMO EL ESTADO DE CADA CONFIGURACION
 * DEL PROGRAMA
 */


/*==================[inclusions]=============================================*/
#include "../inc/MEDIDOR_DISTANCIA_US.h"       /* <= own header */

#include "switch.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "bool.h"
#include "delay.h"
#include "led.h"

/*==================[macros and definitions]=================================*/
#define RETARDO 100
#define BLANCO 0
#define ANTI_REBOTE 100

/*==================[internal data definition]===============================*/
	bool estado_T1=false;											//BANDERA DE LA TECLA 1
	bool unidad=true;												//BANDERA DE LA UNIDAD TRUE= CM .... FALSE=INCHES
	bool hold=false;												//BANDERA DE LA TECLA HOLD

/*==================[internal functions declaration]=========================*/

	//CAMBIA EL ESTADO DE MI BANDERA TECLA 1
void MedicionAct(void){
	estado_T1=!estado_T1;
}
	//CAMBIA EL ESTADO DE MI BANDERA HOLD
void Hold(void){
	hold=!hold;
}
	//CAMBIA EL ESTADO DE MI BANDERA A CM
void UnitCm(void){
	unidad=true;
}
	//CAMBIA EL ESTADO DE MI BANDERA A PULGADAS
void UnitInches(void){
	unidad=false;
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){


	//INICIALIZACION DE LOS DRIVERS DE DISPOSITIVOS
	SystemClockInit();
	HcSr04Init(T_FIL2,T_FIL3); 									// inicializamos el sensor con los pines correspondientes
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5};
	ITSE0803Init(pines);	 									//inicializamos los pines para la pantalla
	SwitchesInit();												//inicializamos los switches
	LedsInit();													//inicializamos switches y leds

	//DECLARAMOS LAS VARIABLES A UTILIZAR
	uint16_t medida=0;											//distancia que censa mi sonar

	//USAMOS LAS INTERRUPCIONES

	SwitchActivInt(SWITCH_1, MedicionAct);						//Interrupcion de la tecla de activacion del sonar
	SwitchActivInt(SWITCH_2, Hold);								//Interrupcion de la tecla Hold
	SwitchActivInt(SWITCH_3, UnitCm);							//Interrupcion que comienza a medir en centimetros
	SwitchActivInt(SWITCH_4, UnitInches);						//Interrupcion que comienza a medir la distancia en pulgadas

    while(1){

    	if(hold==false){										//verifica que no este activada la tecla HOLD
    		if(estado_T1==true){   								//verifica que se haya activado el dispositivo
    			//se prende el led RGB
    			LedOn(LED_RGB_B);
    			LedOn(LED_RGB_R);
    			LedOn(LED_RGB_G);
    			if(unidad==true){								//verifica que la distancia sea medida en centimetros
    				LedOff(LED_3);								//apaga el LED que indica la medicion en pulgadas
    				DelayMs(RETARDO);							//CON ESTE DELAY EVITAMOS QUE LA PANTALLA SE REFRESQUE MUY RAPIDO
    				medida=HcSr04ReadDistanceCentimeters();		//LEEMOS LA MEDIDA
    				LedOn(LED_2);								//prendemos el LED indicador de cm
    			}
    			else{
    				LedOff(LED_2);								//apagamos el led indicador de cm
    				DelayMs(RETARDO);							//evitamos que la pantalla se refresque muy rapido
    				medida=HcSr04ReadDistanceInches();			//leemos la medida en pulgadas
    				LedOn(LED_3);								//prendemos el led indicador de pulgadas
    				}
    		ITSE0803DisplayValue(medida);						//mostramos la medida por el display
    		}//cierre if(estado_T1==true)
    		else{												//Se "apaga" el dispositivo
    		LedOff(LED_RGB_B);
    		LedOff(LED_RGB_G);
    		LedOff(LED_RGB_R);
    		ITSE0803DisplayValue(BLANCO);						//Se resetea el display
    		}
    	LedOff(LED_1);											//Se apaga el led correspondiente al HOLD
    	}//cierre del if(hold==false)
    	else{
    		LedOn(LED_1);										//Se prende el LED correspondiente al HOLD
    		ITSE0803DisplayValue(medida);						//Se muestra el valor guardado anteriormente en el display
    		}

    }//cierre del while(1)




    
	return 0;
}

/*==================[end of file]============================================*/


