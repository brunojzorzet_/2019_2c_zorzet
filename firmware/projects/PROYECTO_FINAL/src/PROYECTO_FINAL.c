/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/PROYECTO_FINAL.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "ili9341.h"
#include "timer.h"
#include "switch.h"
#include "gpio.h"
#include "analog_io.h"
#include "timer.h"
#include "stdint.h"
#include "bool.h"
#include "spi.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define SPI_MISO 44 					//SE CONFIGURA EL PIN DEL SPI
#define CS GPIO1						//SE DEFINE EL PIN DEL GPIO CHIP SELECT
#define DC GPIO3						//SE DEFINE EL PIN QUE SERA USADO PARA EJECUTAR COMANDOS
#define RST GPIO5						//SE DEFINE EL PIN QUE SERÁ USADO PARA RESETEAR LA PANTALLA
#define PERIODO 20						//PERIODO DEL TIMER
#define NULL 0
#define ALTURA_BAR 30 					//ALTO BARRA
/*MARGENES QUE LIMITAN MI AREA DE JUEGO*/
#define PTO_SUP_PANT 240
#define PTO_INF_PANT 1
#define PTO_DER_PANT 320

//P0 INDICA EL PUNTO SUPERIOR, X E Y SON LOS LAS COORDENADAS
//P1 INDICA EL PUNTO INFERIoR, X E Y SON LOS LAS COORDENADAS

//COORDENADAS BARRA IZQUIERDA INICIAL
#define PX0_BAR_IZQ 4
#define PY0_BAR_IZQ 135
#define PX1_BAR_IZQ 9		//7
#define PY1_BAR_IZQ 105
//COORDENADAS BARRA DERECHA INICIAL
#define PX0_BAR_DER 311		//311
#define PY0_BAR_DER 135
#define PX1_BAR_DER 316
#define PY1_BAR_DER 105
//COORDENADAS PELOTA INICIAL
#define CX0 160
#define CY0 120
#define RADIO 5 						//RADIO DE LA PELOTA

#define COLOR_FONDO ILI9341_BLACK		//COLOR DE FONDO (NEGRO)
#define COLOR_BARRA ILI9341_WHITE		//COLOR DE BARRA (BLANCO)
#define COLOR_BALL ILI9341_WHITE		//COLOR DE PELOTA (BLANCO)

/*FACTOR DE ESCALA, PARA REGULAR LOS MANDOS,
 * DEPENDE DEL TAMAÑO DE LA PANTALLA DE JUEGO Y DEL VOLTAJE MAXIMO DE LOS POTENCIOMETROS (3.3V).
 * A sabiendas de que la el alto de la pantalla jugable es de 222, y que el valor de los potenciometros puede ir de 3.3V a 0V.
 * Al asignarle el valor de 3.3V a LIM_SUP y al asignarle 0V a LIM_INF, al realizar la funcion
 * lineal correspondientes a estos dos puntos hallamos el factor de escala*/
#define FACTOR_ESCALA 0.205


#define MITAD_PANTALLA 118				//VALOR DE MITAD DE LA PANTALLA
#define LIM_SUP 231						//LIMITE SUPERIOR DE COMPARACION DE LA PELOTA
#define LIM_INF 9						//LIMITE INFERIOR DE COMPARACION DE LA PELOTA

// COORDENADAS DE LOS PUNTAJES DE LOS JUGADORES
#define DIG 2
//PUNTAJE JUGADOR IZQUIERDO
#define PTJ_IZQ_X0 145
#define PTJ_IZQ_Y0 229
//PUNTAJE JUGADOR DERECHO
#define PTJ_DER_X0 167
#define PTJ_DER_Y0 229

#define V0_X_BALL 1					//VELOCIDAD INICIAL EN X DE MI PELOTA
#define V0_Y_BALL 1					//VELOCIDAD INICIAL EN Y DE MI PELOTA
#define ACELERACION	1					//ACELERACION

/*REALIZAMOS UN STRUCT BARRA PARA TRABAJAR MAS COMODOS, REPRESENTA LAS BARRAS*/
typedef struct {
		uint16_t px0;					//punto superior izquierdo de la barra
		uint16_t py0;
		uint16_t px1;					//punto inferior derecho de la barra
		uint16_t py1;
		uint16_t color;

} barra;

/*REALIZAMOS UN STRUCT BALL PARA TRABAJAR MAS COMODOS, REPRESENTA MI PELOTA*/
typedef struct{
	uint16_t cx;
	uint16_t cy;
	uint16_t radio;
	uint16_t color;
} ball;
/*REALIZAMOS UN STRUCT VELOCIDAD PARA TRABAJAR MAS COMODOS, REPRESENTA LA VELOCIDAD DE LA PELOTA*/
/*Para darle la velocidad a mi pelota decidimos moverla en la dimension horizontal y la direccion vertical*/
typedef struct{
	uint16_t x;
	uint16_t y;
} velocidad;



void Inicio(void);						//FUNCION LLAMADA POR LA INTERRUPCION DEL SWITCH
void Blanco(void);						//FUNCION QUE BLANQUEA LA POSICION DE LAS BARRAS Y PELOTA
void Graficar(void); 					//FUNCION QUE GRAFICA LA POSICION DE LAS BARRAS Y PELOTA
void LecturaControles(void);			//LECTURA DE LOS CONTROLES (CONVERSION A/D)
void Refresco(void);					//FUNCION LLAMADA POR LA INTERRUPCION DEL TIMER
void CalculoPosiciones(void); 			//CALCULA LAS POSICIONES DE MI BARRA Y PELOTA
void Reset(void);						//RESETEA LA POSICION DE MI PELOTA


/*==================[internal data definition]===============================*/

//Configuramos los conversores A/D para mis dos controloes
analog_input_config CONTROL_I={CH1,AINPUTS_SINGLE_READ,NULL}; 	//CONTROL IZQUIERDO
analog_input_config CONTROL_D={CH2,AINPUTS_SINGLE_READ,NULL};	//CONTROL DERECHO

/*Configuramos el timer*/
/*timer_config necesita recibir el tipo de timer, el TIMER A es el Systick, el periodo entre interrupciones(ms),
 * y un puntero a funcion de la funcion que debe ejecutar en cada interrupcion */
timer_config refresh={TIMER_B,PERIODO,Refresco};

/*Definimos nuestros elementos de juego, BARRA IZQUIERDA,BARRA DERECHA y PELOTA*/

barra Bar_I={PX0_BAR_IZQ,PY0_BAR_IZQ,PX1_BAR_IZQ,PY1_BAR_IZQ,COLOR_BARRA};    	//RECIBE LA POSICION DE LA BARRA, Y EL COLOR
barra Bar_D={PX0_BAR_DER,PY0_BAR_DER,PX1_BAR_DER,PY1_BAR_DER,COLOR_BARRA};		//RECIBE LA POSICION DE LA BARRA, Y EL COLOR
ball pelotita={ CX0 , CY0 ,RADIO,COLOR_BALL};									//RECIBE LA POSICION DE LA PELOTA, Y EL COLOR
velocidad V={V0_X_BALL,V0_Y_BALL};												//VELOCIDAD INICIAL A LA CUAL SE MUEVE LA PELOTA

/*Variables de las posiciones de los controles,
 *ES EL VALOR QUE SE MUEVE LA BARRA, DEPENDE DE LA POSICION DE LOS POTENCIOMETROS Y EL FACTOR DE ESCALADO*/
uint16_t CtrI=0;												//CONTROL IZQUIERDO
uint16_t CtrD=0;												//CONTROL DERECHO

/*PUNTAJE DE LOS JUGADORES*/
uint16_t Puntaje_I=0;											//PUNTAJE IZQUIERDO
uint16_t Puntaje_D=0;											//PUNTAJE DERECHO

/*BANDERAS*/
bool refresco=false; 											//Se utiliza para poder comenzar con mis proceso de calculo y graficacion
bool reset=false;												//Se utiliza para volver a reiniciar el juego luego de que ocurra un cambio de puntaje
bool comienzo=false;											//Se utiliza para iniciar el juego al apretar el switch 1

/*VALORES AUXILIARES*/
/*SE PUEDEN UTILIZAR EN UNA MEJORA DEL CODIGO PARA EVITAR QUE LA PELOTA ME BORRE EL PUNTAJE CUANDO ESTA PASE POR ESTOS, NO ESTA IMPLENTADO
 * EN ESTE CODIGO*/
uint8_t punt_aux_izq=0;
uint8_t punt_aux_der=0;

/*==================[internal functions declaration]=========================*/

/*FUNCION LLAMADA POR LA INTERRUPCION DEL SWITCH
 * Al apretarse la tecla 1, lo que ocurre es que se ejecuta la interrupcion que llama a mi funcion Inicio()
 * la cual cambia el estado de mi bandera comienzo, e inicializa o finaliza el Timer*/
void Inicio(void){
	if(comienzo==false){
		comienzo=true;
		TimerStart(refresh.timer);}
	else{
		comienzo=false;
		TimerStop(refresh.timer);
	}
}
/*FUNCION LLAMADA POR LA INTERRUPCION DEL TIMER
 * Esta funcion, Refresco(), es llamada por la interrupcion del Timer, la cual  cambia el estado de mi bandera refrezco
 * la bandera refresco cambia a false luego de completar cada ciclo de while() dentro del main() */
void Refresco(void){
	refresco=true;
}

/*FUNCION QUE BLANQUEA LA POSICION DE LAS BARRAS Y PELOTA
 * Esta función no blanquea toda la pantalla sino que pinta en la pantalla la posicion de las barras y pelotita con
 * el color de fondo predeterminado*/
void Blanco(void){
	ILI9341DrawFilledRectangle(Bar_I.px0,  Bar_I.py0, Bar_I.px1, Bar_I.py1, COLOR_FONDO);
	ILI9341DrawFilledRectangle(Bar_D.px0,  Bar_D.py0, Bar_D.px1, Bar_D.py1, COLOR_FONDO);
	ILI9341DrawFilledCircle(pelotita.cx, pelotita.cy, pelotita.radio,COLOR_FONDO);
	ILI9341DrawLine(PTO_DER_PANT,PTO_SUP_PANT, PTO_DER_PANT,PTO_INF_PANT, COLOR_BARRA);
	ILI9341DrawLine(PTO_INF_PANT,PTO_SUP_PANT, PTO_INF_PANT,PTO_INF_PANT, COLOR_BARRA);
}


/*FUNCION QUE GRAFICA LA POSICION DE LAS BARRAS Y PELOTA*/
void Graficar(void){
	ILI9341DrawFilledRectangle(Bar_I.px0,  Bar_I.py0, Bar_I.px1, Bar_I.py1, Bar_I.color);
	ILI9341DrawFilledRectangle(Bar_D.px0,  Bar_D.py0, Bar_D.px1, Bar_D.py1, Bar_D.color);
	ILI9341DrawFilledCircle(pelotita.cx, pelotita.cy, pelotita.radio,pelotita.color);
}

/*LECTURA DE LOS CONTROLES (CONVERSION A/D), lo que realizamos es la conversion de los controles analogicos a digital
 * y luego guardamos el valor en las variables de la posicion de los controles, multiplicandolas por el factor de escala para poder
 * posicionarlas correctamente en el tamaño de nuestra pantalla de juego*/
void LecturaControles(void){
	AnalogInputInit(&CONTROL_I);
	AnalogInputReadPolling(CONTROL_I.input, &CtrI);
	AnalogInputInit(&CONTROL_D);
	AnalogInputReadPolling(CONTROL_D.input, &CtrD);

	//ESCALADO DE LOS CONTROLES
	CtrI=(float)CtrI*FACTOR_ESCALA;
	CtrD=(float)CtrD*FACTOR_ESCALA;
	CtrI=(int16_t)CtrI+ALTURA_BAR;
	CtrD=(int16_t)CtrD+ALTURA_BAR;
	}


/*CALCULA LAS POSICIONES DE MI BARRA Y PELOTA*/
void CalculoPosiciones(void){
	/*TENEMOS QUE SUMAR A LAS POSICIONES DE LA BARRA EL VALOR DEL CTR TENIENDO EN CUENTA EL LIMITE SUPERIOR DE LA BARRA*/
	Bar_I.py0=CtrI;
	/*Restamos la altura de la barra para ubicar el punto inferior de las barras*/
	Bar_I.py1=CtrI-ALTURA_BAR;
	Bar_D.py0=CtrD;
	Bar_D.py1=CtrD-ALTURA_BAR;

	/*Calculo de la posicion de la pelota*/

	//COMPARACION EN Y
	/*Si el limite superior de la pelota es igual al limite superior o igual al limite inferior, se invierte la velocidad en V.y*/
	if(((pelotita.cy+pelotita.radio)>=LIM_SUP)|((pelotita.cy-pelotita.radio)<=LIM_INF)){
		V.y=-V.y;
	}

	//COMPARACION EN X DE LA PELOTITA, BARRA DERECHA
	/*COMPARAMOS LA POSICION EN X DE LA PELOTA CON LA POSICION IZQUIERDA DE MI BARRA DERECHA,
	 * SI LA POSICION DE LA PELOTA SE ENCUENTRA MENOR A ESTE VALOR SIGNIFICA QUE EL JUEGADOR CONTRARIO HIZO UN GOL,
	 * Y SI LA PELOTA NO LLEGA A PASAR EL VALOR DE LA BARRA OCURRE UN REBOTE DE LA PELOTA*/
	if((pelotita.cx+pelotita.radio)>=PX0_BAR_DER){
		if((pelotita.cy<=Bar_D.py0)&(pelotita.cy>=Bar_D.py1)){
			V.x=-V.x;
			if(V.x<0){
				V.x=V.x-ACELERACION;
			}
			else{
				V.x=V.x+ACELERACION;
			}
			}
		else{
			punt_aux_izq=Puntaje_I;
			Puntaje_I=Puntaje_I+1;
			reset=true;									//Se activa la bandera de reset, la cual reinicia mi juego
			}
		}

	//COMPARACION EN X BARRA IZQUIERDA
	/*COMPARAMOS LA POSICION EN X DE LA PELOTA CON LA POSICION DERECHA DE MI BARRA IZQUIERDA,
	 * SI LA POSICION DE LA PELOTA SE ENCUENTRA MENOR A ESTE VALOR SIGNIFICA QUE EL JUEGADOR CONTRARIO HIZO UN GOL,
	 * Y SI LA PELOTA NO LLEGA A PASAR EL VALOR DE LA BARRA OCURRE UN REBOTE DE LA PELOTA*/
	if((pelotita.cx+pelotita.radio+V.x)<=PX1_BAR_IZQ){
		if((pelotita.cy<=Bar_I.py0)&(pelotita.cy>=Bar_I.py1)){
			V.x=-V.x;
			if(V.x<0){
				V.x=V.x-ACELERACION;
			}
			else{
				V.x=V.x+ACELERACION;
			}
		}
		else{
			punt_aux_der=Puntaje_D;
			Puntaje_D=Puntaje_D+1;
			reset=true;
		}
	}

	/*CALCULO DE LA NUEVA POSICION DE LA PELOTA*/
	pelotita.cx=pelotita.cx+V.x;
	pelotita.cy=pelotita.cy+V.y;
}

/*RESETEA DE LA POSICION DE LA PELOTA,
 * se llama a la funcion cada vez comete un gol*/
void Reset(void){
	pelotita.cx=CX0;
	pelotita.cy=CY0;
}

/*Funcion que se encarga de graficar los puntajes de los jugadores*/
void GraficaPuntajes(void){
	ILI9341DrawInt(PTJ_DER_X0, PTJ_DER_Y0, Puntaje_D, DIG, &font_7x10, COLOR_BARRA, COLOR_FONDO);
	ILI9341DrawInt(PTJ_IZQ_X0, PTJ_IZQ_Y0, Puntaje_I, DIG, &font_7x10, COLOR_BARRA, COLOR_FONDO);
}

/*Funcion que blanquea mis puntajes*/
void BlancoPuntaje(void){
	ILI9341DrawFilledRectangle(PTJ_IZQ_X0,  PTJ_IZQ_Y0, PTJ_IZQ_X0+7, PTJ_IZQ_Y0-10, COLOR_FONDO);
	ILI9341DrawFilledRectangle(PTJ_DER_X0,  PTJ_DER_Y0, PTJ_DER_X0+7, PTJ_DER_Y0-10, COLOR_FONDO);
}

int main(void){
	SystemClockInit();
	ILI9341Init(SPI_1,CS, DC,RST);							//Inicializamos la pantalla
	ILI9341Rotate(ILI9341_Landscape_2);						//Rotamos la pantalla para que se corresponda con las coordenadas establecidas por nosotros
	ILI9341Fill(COLOR_FONDO);								//Damos el color de fondo
	ILI9341DrawRectangle(PTO_INF_PANT, PTO_SUP_PANT, PTO_DER_PANT,PTO_INF_PANT, COLOR_BARRA);	//Graficamos el rectangulo que limitara el espacio de juego

	SwitchesInit();											//Inicializamos los switch
	SwitchActivInt(SWITCH_1, Inicio);						//Definimos la interrupcion que se llamara cada vez que se aprete la tecla 1
	TimerInit(&refresh);									//Inicializamos el timer

   while(1){
	   if(refresco==true){
		   LecturaControles();
		   Blanco();
		   CalculoPosiciones();
		   Graficar();
		   if(reset==true){
			   Blanco();
			   GraficaPuntajes();
			   Reset();
			   reset=false;
		   }
		   refresco=false;
	   }

	}
    
	return 0;
}

/*==================[end of file]============================================*/

