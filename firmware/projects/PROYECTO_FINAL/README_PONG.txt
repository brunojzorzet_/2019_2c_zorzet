////////////////........PONG............//////////////
Esta aplicacion es una recreacion del iconico juego comercializado por ATARI
Similar a un tejo con vista superior, donde se comandan dos paletas para hacer revotar una pelota
Para poder jugar es necesario contar con 4 componentes:
	-Placa de desarrollo EDU-CIAA
	-Dos potenciometros lineales, que hacen las veces de controles de las paletas
	-Pantalla LCD ili9341

Se debe conectar los potenciometros en las entradas de conversion analogica digital
Los pines centrales de los potenciometros se conectan a CH1 y CH2, y los pines extremos 
se conectan a VDDA y GNDA.

La pantalla debe conectarse respetando la siguiente tabla:
*
 * |      Display	|      EDU-CIAA	   |
 *

 *  |:-----------------:|:----------------:|
 *

 *  | 	 SDO/MISO 	|	SPI_MISO   |
 *
 
*  | 	 LED		| 	3V3	   |
 *

 *  | 	 SCK		| 	SPI_SCK	   |
 *

 *  | 	 SDI/MOSI 	| 	SPI_MOSI   |
 *

 *  | 	 DC/RS	 	| 	GPIO3	   |
 * 

 *  | 	 RESET	 	| 	GPIO5	   |
 * 

 *  | 	 CS		| 	GPIO1	   |
 * 

 *  | 	 GND		| 	GND	   |
 *

 *  | 	 VCC		| 	3V3	   |
 
*


Para comensar a jugar debera presionar la tecla 1 de la EDU-CIAA
Este mismo boton servira a modo de pausa en el juego
En la pantalla en el margen inferior se indicaran los puntos de cada uno de los jugadores.