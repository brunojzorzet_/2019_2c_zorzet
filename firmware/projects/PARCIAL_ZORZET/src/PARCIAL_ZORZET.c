/*
 * PARCIAL DE ELECTRONICA PROGRAMABLE
 *
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * BJZorzet - bjzorzet@gmail.com
 *
 *
 *
 * Revisión:
 * 28-10-19: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/PARCIAL_ZORZET.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "analog_io.h"
#include "gpio.h"
#include "timer.h"
#include "switch.h"
#include "uart.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/
#define PERIODO 4   											/*Cada 4ms se deben tomar las muestras desde el ch1*/
#define NULL 0
#define CANT_MUESTRAS_1S 250									//ESTE VALOR ES DEBIDO A QUE LA FRECUENCIA DE ADQUISICION DE DATOS ES DE 250 Hz, POR ENDE EN 1 SEGUNDO TENDREMOS 250 DATOS.
#define VAL_INICIAL 0											//Es el valor en el que inician los indices uttilizados para recorrer vectores
#define VEL_TRANSF 115200										//Vewlocidad de transfgerencia de datos
#define P_MAX 1													//Indice que hace referencia a la presion arterial maxima
#define P_MIN 2													//Indice que hace referencia a la presion arterial minima
#define P_PROM 3												//Indice que hace referencia a la presion arterial promedio
#define PRES_MAX_PACIENTE 150									//Valor de presion maxima
#define PRES_MIN_PACIENTE 50									//Valor de presion minima
#define DECIMAL 10												//Valor en decimal


/*CREO UNA VARIABLE DE TIPO PRESION ARTERIAL PARA TRABJAR MAS COMODOS*/
typedef struct{
	uint16_t presiones[CANT_MUESTRAS_1S];
	uint16_t pres_max;
	uint16_t pres_prom;
	uint16_t pres_min;
} presion_arterial;

/*==================[internal functions declaration]=========================*/
void Refresco(void);
void Inicio(void);
uint16_t LecturaCh1(void);
void ActValMax(void);
void ActValMin(void);
void ActValProm(void);
void ValMaximo(void);
void ValPromedio(void);
void ValMinimo(void);
void EnvioPresiones(uint16_t pres_a_enviar);

/*==================[internal data definition]===============================*/
presion_arterial pres_paciente;									//Inicializo mi variable de presion artwerial para el paciente
analog_input_config presion_input={CH1,AINPUTS_SINGLE_READ,NULL}; 	//define la variable que se encargara de la gestion del conversor A/D
timer_config refresh={TIMER_B,PERIODO,Refresco};				//Defino mi timer, el cual llamara luego de 40ms a la funcion Refresco
serial_config ComunicacionPC={SERIAL_PORT_RS485,VEL_TRANSF,NO_INT}; //Definimos el PUERTO DE COMUNICACION SERIE

/*BANDERAS*/
bool refresco=false; 											//Se utiliza para poder comenzar con la lectura/conversion de datos desde mi CH1
bool comienzo=false;											//Se utiliza para iniciar el programa
bool val_max_act=true;											//Se utiliza para que se muestre y envie el valor de presion arterial maxima
bool val_min_act=false;											//Se utiliza para que se muestre y envie el valor de presion arterial minima
bool val_prom_act=false;										//Se utiliza para que se muestre y envie el valor de presion arterial promedio

/*==================[external functions definition]==========================*/
/*FUNCION LLAMADA POR LA INTERRUPCION DEL SWITCH
 * Al apretarse la tecla 1, lo que ocurre es que se ejecuta la interrupcion que llama a mi funcion Inicio()
 * la cual cambia el estado de mi bandera comienzo, e inicializa o finaliza el Timer*/
void Inicio(void){
	if(comienzo==false){
		comienzo=true;
		TimerStart(refresh.timer);}
	else{
		comienzo=false;
		TimerStop(refresh.timer);
	}
}


/*FUNCION LLAMADA POR LA INTERRUPCION DEL TIMER
 * Esta funcion, Refresco(), es llamada por la interrupcion del Timer, la cual  cambia el estado de mi bandera refrezco
 * la bandera refresco cambia a false luego de completar cada ciclo de while() dentro del main() */
void Refresco(void){
	refresco=true;
}

/*LECTURA DEL CH1 (CONVERSION A/D), esta funcion lee lo que hay en el buffer del conversor con este valor realiza el calculo necesario para hallar la presion arterial*/
uint16_t LecturaCh1(void){
	uint16_t pres_aux=0;							//variable auxiliar
	AnalogInputInit(&presion_input);
	AnalogInputReadPolling(presion_input.input, &pres_aux);
	pres_aux=(float)pres_aux*200/3.3;				//calculo de regla de tres simples basandones en que a 3.3V le corresponden 200mmHg y a 0V 0mmHg
	return (uint16_t)pres_aux;
}

/*Funcion que cambia el estado de mi bandera val_max_act*/
void ActValMax(void){
	val_max_act=true;
}
/*Funcion que cambia el estado de mi bandera val_min_act*/
void ActValMin(void){
	val_min_act=true;
}
/*Funcion que cambia el estado de mi bandera val_prom_act*/
void ActValProm(void){
	val_prom_act=true;
}

/*Funcion que calcula el valor de mi presion arterial maxima*/
 void ValMaximo(void){
	float max_value=pres_paciente.presiones[VAL_INICIAL];
	uint16_t ind_aux=1;
	for (ind_aux;ind_aux++;CANT_MUESTRAS_1S-1){
		if(max_value<pres_paciente.presiones[ind_aux])
			max_value=pres_paciente.presiones[ind_aux];
		}
	pres_paciente.pres_max=max_value;
}
/*Funcion que calcula el valor de mi presion arterial promedio*/
 void ValPromedio(void){
	float prom_value=pres_paciente.presiones[VAL_INICIAL];
	uint16_t ind_aux=1;
	for (ind_aux;ind_aux++;CANT_MUESTRAS_1S-1){
		prom_value+=pres_paciente.presiones[ind_aux];
		}
	pres_paciente.pres_prom=(uint16_t)(prom_value/CANT_MUESTRAS_1S);
}
 /*Funcion que calcula el valor de mi presion arterial minima*/
 void ValMinimo(void){
	float min_value=pres_paciente.presiones[VAL_INICIAL];
	uint16_t ind_aux=1;
	for (ind_aux;ind_aux++;CANT_MUESTRAS_1S-1){
		if(min_value>pres_paciente.presiones[ind_aux])
			min_value=pres_paciente.presiones[ind_aux];
		}
	pres_paciente.pres_min=min_value;
}
 /*Funcion que envia el valor de mi presion arterial al dysplay y a la PC*/
void EnvioPresiones(uint16_t pres_a_enviar){

	switch(pres_a_enviar){
		case P_MAX:
			UartSendString(ComunicacionPC.port,UartItoa(pres_paciente.pres_max,DECIMAL));
			UartSendString(ComunicacionPC.port," mmHg \r\n");
			ITSE0803DisplayValue(pres_paciente.pres_max);
		case P_MIN:
			UartSendString(ComunicacionPC.port,UartItoa(pres_paciente.pres_min,DECIMAL));
			UartSendString(ComunicacionPC.port," mmHg \r\n");
			ITSE0803DisplayValue(pres_paciente.pres_min);
		case P_PROM:
			UartSendString(ComunicacionPC.port,UartItoa(pres_paciente.pres_prom,DECIMAL));
			UartSendString(ComunicacionPC.port," mmHg \r\n");
			ITSE0803DisplayValue(pres_paciente.pres_prom);
	}


}



int main(void)
{
	SystemClockInit();
	LedsInit();//Se inicializa el led es fundamental ya que de esta manera le decimos a los perifericos que se inicializa como salida el led numero tanto
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5};				//definimos los pines que utilizara mi display
	ITSE0803Init(pines);	 											//inicializamos los pines para la pantalla
	SwitchesInit();


	uint16_t ind_pres=0;												//Este indice lo utilizo para contar la cantidad de datos que recibo

	SwitchActivInt(SWITCH_1, Inicio);  //Al apretar la tecla 1 se genera el incio del programa
	SwitchActivInt(SWITCH_2, ActValMax);//Al apretar la tecla 2 se activa la lectura con el valor maximo de presion arterial
	SwitchActivInt(SWITCH_3, ActValMin);//Al apretar la tecla 3 se activa la lectura con el valor minimo de presion arterial
	SwitchActivInt(SWITCH_4, ActValProm);//Al apretar la tecla 2 se activa la lectura con el valor prom de presion arterial




	while(1){
		if(comienzo==true){
			if(refresco==true){
				pres_paciente.presiones[ind_pres]=LecturaCh1();
				ind_pres+=1;
			}
			if(ind_pres==CANT_MUESTRAS_1S-1){							//Cuando ind_pres llega a 249 significa que paso 1 segundo de recoleccion de datos
    			ind_pres=0;												//Se iguala a cero para que empiece un nuevo recuento del segundo
    			ValMaximo();
    			ValPromedio();
    			ValMinimo();
    			if(val_min_act==true){
    				EnvioPresiones(P_MIN);
    				LedOn(LED_1);										//Se prende en LED 1 indicando que se presenta por el display el valor minimo
    			}
    			if(val_max_act==true){
    				EnvioPresiones(P_MAX);
    				LedOn(LED_2);										//Se prende en LED 2 indicando que se presenta por el display el valor maximo
    			}
    			if(val_prom_act==true){
    				EnvioPresiones(P_PROM);
    				LedOn(LED_3);										//Se prende en LED 2 indicando que se presenta por el display el valor maximo
    			}
    			if(pres_paciente.pres_max>=PRES_MAX_PACIENTE){
    				LedOn(LED_RGB_R);
    			}
    			if(pres_paciente.pres_max>=PRES_MIN_PACIENTE && pres_paciente.pres_max<=PRES_MAX_PACIENTE){
    				LedOn(LED_RGB_B);
    			}
    			if(pres_paciente.pres_max<=PRES_MIN_PACIENTE){
    				LedOn(LED_RGB_G);
    			}
			}

			refresco=false;
		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/

