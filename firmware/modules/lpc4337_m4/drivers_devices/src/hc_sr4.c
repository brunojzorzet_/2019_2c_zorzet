/*
 * hc_sr4.c
 *
 *  Created on: 29 ago. 2019
 *      Author: Usuario
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "stdint.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/
gpio_t echo_global;//SEÑAL DE ECO QUE SE RECIBE
gpio_t trigger_global; //SEÑAL DE DISPARO DE PULSO

#define ANCHO_PULSO 10 //DEFINIMOS EL ANCHO DEL PULSO TRIGGER
#define RETARDO 1//ESTE RETARDO LO UTILIZAMOS PARA AJUSTAR LA FRECUENCIA DE CONTEO



/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/



//INICIALIZAMOS LOS PUERTOS
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	/** Configuration of the GPIO */
	echo_global=echo;
	trigger_global=trigger;

	GPIOInit(echo_global, GPIO_INPUT);
	GPIOInit(trigger_global, GPIO_OUTPUT);

	return true;
}

int16_t HcSr04ReadDistanceInches(void){


	GPIOOn(trigger_global);								//MANDAMOS UN PULSO ALTO DE 15useg AL TRIGGER DEL DISPOSITIVO
	DelayUs(ANCHO_PULSO);
	GPIOOff(trigger_global);

	int16_t DIST=0;
		while(GPIORead(echo_global)==false){			//ESPERAMOS HASTA QUE LA SEÑAL DEL ECHO PASE A ALTO,
		DelayUs(RETARDO);
	}
		while(GPIORead(echo_global)==true){				//LUEGO COMENZAMOS EL CONTEO HASTA QUE CAMBIE DE ESTADO
		DIST+=1;
		DelayUs(RETARDO);
	}



	DIST=DIST/148;										//CALCULAMOS LA DISTANCIA CON LA FORMULA PRESENTADA EN LA HOPJA DE DATOS DEL SISTEMA

	return DIST;}




int16_t HcSr04ReadDistanceCentimeters(void){


	GPIOOn(trigger_global);								//MANDAMOS UN PULSO ALTO DE 15useg AL TRIGGER DEL DISPOSITIVO
	DelayUs(ANCHO_PULSO);
	GPIOOff(trigger_global);

	int16_t DIST=0;
		while(GPIORead(echo_global)==false){			//ESPERAMOS HASTA QUE LA SEÑAL DEL ECHO PASE A ALTO,
		}
		while(GPIORead(echo_global)==true){				//LUEGO COMENZAMOS EL CONTEO HASTA QUE CAMBIE DE ESTADO
		DIST+=1;
		DelayUs(RETARDO);
	}



	DIST=DIST/29;										//CALCULAMOS LA DISTANCIA CON LA FORMULA PRESENTADA EN LA HOPJA DE DATOS DEL SISTEMA

	return DIST;}






bool HcSr04Deinit(gpio_t echo, gpio_t trigger){

	return true;
}
